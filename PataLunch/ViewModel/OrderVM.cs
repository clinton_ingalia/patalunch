﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulaLunch.ViewModel
{
    public class OrderVM
    {
        public Guid SessionId { get; set; }
        public string FullNames { get; set; }
        public string LocationName { get; set; }
        public string BuldingName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Paid { get; set; }
    }
}