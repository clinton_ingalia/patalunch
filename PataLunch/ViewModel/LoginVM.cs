﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulaLunch.ViewModel
{
    public class LoginVM
    {
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }
}