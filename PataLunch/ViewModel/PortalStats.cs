﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulaLunch.ViewModel
{
    public class PortalStats
    {
        public int todaysOrders { get; set; }
        public int Users { get; set; }
        public int Orders { get; set; }
        public int Menu { get; set; }
    }
}