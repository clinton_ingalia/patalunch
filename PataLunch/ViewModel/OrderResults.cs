﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulaLunch.ViewModel
{
    public class OrderResults
    {
        public Guid OrderId { get; set; }
        public string Names { get; set; }
        public string PhoneNumber { get; set; }
        public string LocationName { get; set; }
        public int FloorNo { get; set; }
        public string Office { get; set; }
        public string DishName { get; set; }
        public decimal Price { get; set; }
        public DateTime Timestamp { get; set; }
    }
}