﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulaLunch.ViewModel
{
    public class LoginResults
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LocationName { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int FloorNo { get; set; }
        public string Office { get; set; }
    }
}