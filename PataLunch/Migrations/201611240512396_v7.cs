namespace KulaLunch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "FullNames", c => c.String());
            AddColumn("dbo.Orders", "LocationName", c => c.String());
            AddColumn("dbo.Orders", "BuldingName", c => c.String());
            AddColumn("dbo.Orders", "Name", c => c.String());
            AddColumn("dbo.Orders", "Description", c => c.String());
            AddColumn("dbo.Orders", "Price", c => c.Int(nullable: false));
            DropColumn("dbo.Orders", "MenuId");
            DropColumn("dbo.Orders", "MenuId0");
            DropColumn("dbo.Orders", "MenuId2");
            DropColumn("dbo.Orders", "MenuId3");
            DropColumn("dbo.Orders", "MenuId4");
            DropColumn("dbo.Orders", "MenuId5");
            DropColumn("dbo.Orders", "MenuId6");
            DropColumn("dbo.Orders", "MenuId7");
            DropColumn("dbo.Orders", "MenuId8");
            DropColumn("dbo.Orders", "MenuId9");
            DropColumn("dbo.Orders", "MenuId1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "MenuId1", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId9", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId8", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId7", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId6", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId5", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId4", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId3", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId2", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId0", c => c.Guid(nullable: false));
            AddColumn("dbo.Orders", "MenuId", c => c.Guid(nullable: false));
            DropColumn("dbo.Orders", "Price");
            DropColumn("dbo.Orders", "Description");
            DropColumn("dbo.Orders", "Name");
            DropColumn("dbo.Orders", "BuldingName");
            DropColumn("dbo.Orders", "LocationName");
            DropColumn("dbo.Orders", "FullNames");
        }
    }
}
