namespace KulaLunch.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "MenuId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "MenuId", c => c.Guid(nullable: false));
        }
    }
}
