'use strict';
angular.module('KulaLunch', ["ngStorage", "angular-growl", "ngProgress", "uiGmapgoogle-maps", "ngJsonExportExcel"])
    .config(['growlProvider', function(growlProvider) {
        growlProvider.globalTimeToLive({success: 1000, error: 2000, warning: 3000, info: 4000});
    }])
    .filter('unique', function () {
        return function (collection, keyname) {
            var output = [],
                keys = [];

            angular.forEach(collection, function (item) {
                var key = item[keyname];
                if (keys.indexOf(key) === -1) {
                    keys.push(key);
                    output.push(item);
                }
            });

            return output;
        };
    })
    .factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
    .controller("indexController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory) {
        //instance of progress bar
        $scope.progressbar = ngProgressFactory.createInstance();
        //check for session
        if ($localStorage.UserId == null) {
            $scope.UM = "REGISTER/LOGIN";
        } else {
            //check session if expired!
            $scope.progressbar.start();
            $http.get('http://kulalunch.azurewebsites.net/api/SessionsByUserId?id=' + $localStorage.UserId)
                 .then(function (response) {
                     // Request completed successfully
                     console.log(response);
                     $localStorage.SessionId = response.data["SessionId"];
                     $scope.UM = "LOGOUT";
                     $scope.fn = $localStorage.FirstName;
                     $scope.ln = $localStorage.LastName;
                     $scope.bn = $localStorage.BuildingName;
                     $scope.of = $localStorage.Office;
                     $scope.fno = $localStorage.FloorNo;
                     $scope.pnumber = $localStorage.PhoneNumber;
                     $scope.em = $localStorage.Email;
                     $scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };
                     $scope.lg = false;
                     $scope.loginForm = true;
                     $scope.editForm = true;
                     $scope.rForm = true;
                     $scope.progressbar.complete();
                     growl.info("Welcome back " + $localStorage.FirstName);
                     $scope.History();
                 }, function (x) {
                     // Request error
                     console.log(x);
                     $scope.lg = true;
                     $scope.loginForm = false;
                     $scope.rForm = true;
                     $scope.UM = "LOGIN";
                     $scope.progressbar.complete();
                     growl.warning("Session has expired, please login again");
                 });
        }

        //orderlist
        $scope.orderItems = [];
        $scope.on = 0;

        //get menu
        $http.get('http://kulalunch.azurewebsites.net/api/menus')
                .then(function (response) {
                    // Request completed successfully
                    console.log(response);
                    $scope.ditems = response.data;
                    var d = new Date();
                    if (d.getDay != 5) {
                        for (var i = $scope.ditems.length - 1; i >= 0; i--) {
                            if ($scope.ditems[i].MenuId == 'f3db6bed-b87d-4cd5-b0ae-8fbc5e34ca1c') {
                                $scope.ditems.splice(i, 1);
                            }
                        }
                        //$scope.items = $filter('filter')($scope.items, { MenuId: 'f3db6bed-b87d-4cd5-b0ae-8fbc5e34ca1c' });
                    }
                    $scope.items = $scope.ditems;
                }, function (x) {
                    // Request error
                    console.log(x);
                    growl.error("Could not retrieve menu items");
                });

        //total price
        $scope.tPrice = 0;
        //$scope.PC = 0;

        //orderItem 

        $scope.mOrders = [
           
        ]

        $scope.viewOrders = function () {
            $scope.orderTable = false;
            $scope.historyTable = true;
            $scope.vOrdersBtn = true;
            $scope.submitBtn = false;
            $scope.historysBtn = false;
        }

        $scope.orderTable = false;
        $scope.historyTable = true;
        $scope.vOrdersBtn = true;
        $scope.submitBtn = false;
        $scope.historysBtn = false;
        //history
        $scope.History = function () {
            if ($localStorage.UserId != null) {
                $scope.vOrdersBtn = false;
                $scope.orderTable = true;
                $scope.historyTable = false;
                $scope.submitBtn = true;
                $scope.historysBtn = true;
                $http.get('http://kulalunch.azurewebsites.net/api/History?names=' + $localStorage.FirstName + " " + $localStorage.LastName)
                   .then(function (response) {
                       // Request completed successfully
                       //if (response != null) {
                           console.log(response);
                           growl.info("You have an order placed, kindly view history tab on orders.");
                           $scope.Historyitems = response.data;
                       
                       
                       
                   }, function (x) {
                       // Request error
                       if (x.status == 404) {
                           growl.info("You have not placed any order today.");
                       } else {
                           growl.error("Could not retrieve history items, please try again");
                       }
                       console.log(x);
                       
                   });
            } else {
                growl.error("Kindly login to view History");
            }
           
        }

        $scope.AddChapati = function () {
            //$scope.progressbar.start();
            $scope.mOrders.push({
                Name: "1 Chapati", Description: "Chapati", Price: 40, SessionId: $localStorage.SessionId,
                FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                LocationName: $localStorage.Office,
                BuldingName: $localStorage.BuildingName, Paid: 0
            });

            $scope.tPrice = $scope.tPrice + 40;
            //$scope.$apply();
            $scope.on = $scope.on + 1;
            //$scope.progressbar.complete();
            growl.success("1 Chapati added to orders");
        }

        $scope.AddPlainRice = function () {
            //$scope.progressbar.start();
            $scope.mOrders.push({
                Name: "1 Plain Rice", Description: "Plain Rice", Price: 50, SessionId: $localStorage.SessionId,
                FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                LocationName: $localStorage.Office,
                BuldingName: $localStorage.BuildingName, Paid: 0
            });

            $scope.tPrice = $scope.tPrice + 50;
            //$scope.$apply();
            $scope.on = $scope.on + 1;
            //$scope.progressbar.complete();
            growl.success("1 Plain Rice added to orders");
        }

        $scope.AddRiceBiryani = function () {
            //$scope.progressbar.start();
            $scope.mOrders.push({
                Name: "1 Rice Biryani", Description: "Rice Biryani", Price: 50, SessionId: $localStorage.SessionId,
                FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                LocationName: $localStorage.Office,
                BuldingName: $localStorage.BuildingName, Paid: 0
            });

            $scope.tPrice = $scope.tPrice + 50;
            //$scope.$apply();
            $scope.on = $scope.on + 1;
            //$scope.progressbar.complete();
            growl.success("1 Rice Biryani added to orders");
        }

        $scope.AddPilau = function () {
            //$scope.progressbar.start();
            $scope.mOrders.push({
                Name: "1 Pilau", Description: "Pilau", Price: 50, SessionId: $localStorage.SessionId,
                FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                LocationName: $localStorage.Office,
                BuldingName: $localStorage.BuildingName, Paid: 0
            });

            $scope.tPrice = $scope.tPrice + 50;
            //$scope.$apply();
            $scope.on = $scope.on + 1;
            //$scope.progressbar.complete();
            growl.success("1 Pilau added to orders");
        }

        //add item to list
        $scope.addItem = function (item, PC) {
            $scope.progressbar.start();
            if ($scope.on <= 10) {
                if (PC == 1) {
                    $scope.mOrders.push({
                        Name: "Plain Rice & " + item.Name, Description: item.Description, Price: item.Price, SessionId: $localStorage.SessionId,
                        FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                        LocationName: $localStorage.Office,
                        BuldingName: $localStorage.BuildingName, Paid: 0
                    });

                    $scope.tPrice = $scope.tPrice + item.Price;
                    //$scope.$apply();
                    $scope.on = $scope.on + 1;
                    $scope.progressbar.complete();
                    growl.success("Plain Rice & " + item.Name + " added to orders");
                } else if (PC == 2) {
                    $scope.mOrders.push({
                        Name: "Rice Biryani & " + item.Name, Description: item.Description, Price: item.Price, SessionId: $localStorage.SessionId,
                        FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                        LocationName: $localStorage.Office,
                        BuldingName: $localStorage.BuildingName, Paid: 0
                    });

                    $scope.tPrice = $scope.tPrice + item.Price;
                    //$scope.$apply();
                    $scope.on = $scope.on + 1;
                    $scope.progressbar.complete();
                    growl.success("Rice Biryani & " + item.Name +" added to orders");
                } else if (PC == 3) {
                    
                    $scope.mOrders.push({
                        Name: "Pilau & " + item.Name, Description: item.Description, Price: item.Price, SessionId: $localStorage.SessionId,
                        FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                        LocationName: $localStorage.Office,
                        BuldingName: $localStorage.BuildingName, Paid: 0
                    });

                    $scope.tPrice = $scope.tPrice + item.Price;
                    //$scope.$apply();
                    $scope.on = $scope.on + 1;
                    $scope.progressbar.complete();
                    growl.success("Pilau & " + item.Name + " added to orders");
                } else if (PC == 4) {
                    $scope.mOrders.push({
                        Name: "Chapati & " + item.Name, Description: item.Description, Price: item.Price, SessionId: $localStorage.SessionId,
                        FullNames: $localStorage.FirstName + " " + $localStorage.LastName,
                        LocationName: $localStorage.Office,
                        BuldingName: $localStorage.BuildingName, Paid: 0
                    });

                    $scope.tPrice = $scope.tPrice + item.Price;
                    //$scope.$apply();
                    $scope.on = $scope.on + 1;
                    $scope.progressbar.complete();
                    growl.success("Chapati & " + item.Name + " added to orders");
                }

                console.log($scope.mOrders);
                
            } else {
                $scope.progressbar.complete();
                growl.error("You can only have 10 items Per Account");
            }
            
        }

        //remove item from list
        $scope.removeItem = function (item) {
            //$scope.orderItems.push(item);
            $scope.progressbar.start();
            var index = $scope.mOrders.indexOf(item);
            $scope.mOrders.splice(index, 1);
            console.log(item);
            //angular.forEach($scope.orderItems, function (value, key) {
            //    console.log(value.MenuId);
            //    if (value.MenuId == item.MenuId)
            //        console.log("username is thomas");
            //});
            //console.log(item);
            //console.log($scope.orderItems);
            $scope.tPrice = $scope.tPrice - item.Price;
            $scope.on = $scope.on - 1;
            $scope.progressbar.complete();
            growl.error(item.Name + " removed");
        }

        

        //user model
        $scope.User = {
            FirstName: "",
            LastName: "",
            PhoneNumber: "",
            Password: "",
            Email: "",
            LocationName: "",
            Latitude: $localStorage.Latitude,
            Longitude: $localStorage.Longitude,
            FloorNo: 9,
            Office: ""
        }

        $scope.UserLogin = {
            PhoneNumber: "",
            Password: ""
        }

        //get location method
        // $scope.GL = "GET LOCATION";
        $scope.getLocation = function () {
            // $scope.GL = "PROCESSING...";
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {

                    $localStorage.Latitude = position.coords.latitude;
                    $localStorage.Longitude = position.coords.longitude;
                    
                    //console.log(position.coords.latitude);
                    //console.log(position.coords.longitude);
                });

                console.log($localStorage.Latitude);
                console.log($localStorage.Longitude);
            
                // $scope.GL = "Location Acquired!";
            }
        }
        
        $scope.editForm = true;
        $scope.lg = true;
        $scope.loginForm = true;
        $scope.rForm = false;
        $scope.UMA = "Login";
        $scope.Reg = "Register";
        $scope.Register = function (User) {
            if (User.PhoneNumber.charAt(0) === '0' || User.PhoneNumber.charAt(0) === '+') {
                $scope.progressbar.start();
                $scope.getLocation();
                if (!User.PhoneNumber === '+') {
                    User.PhoneNumber = "254" + User.PhoneNumber.slice(1);
                }
                $http.post('http://kulalunch.azurewebsites.net/api/Users', User)
                  .then(function (response) {
                      // Request completed successfully
                      console.log(response);
                      $scope.progressbar.complete();
                      //$scope.Reg = "Welcome, Kindly Login";
                      growl.success("Welcome, Kindly Login");
                      $scope.UM = "Login"
                      $scope.loginForm = false;
                      $scope.rForm = true;
                      $scope.lg = true;
                  }, function (x) {
                      // Request error
                      console.log(x);
                      $scope.progressbar.complete();
                      growl.error("Error Could not Register");
                      // $scope.Reg = "Error Could not Register";
                  });
            } else {
                growl.error("Phone number is incorrect format");
            }
            
           
        }

        $scope.UpdateUser = function (User) {
            $scope.progressbar.start();
            $scope.getLocation();
            $http.put('http://kulalunch.azurewebsites.net/api/Users/' + $localStorage.UserId, User)
                  .then(function (response) {
                      // Request completed successfully
                      console.log(response);
                      $scope.progressbar.complete();
                      //$scope.Reg = "Welcome, Kindly Login";
                      growl.success("Your changes have been saved");
                  }, function (x) {
                      // Request error
                      console.log(x);
                      $scope.progressbar.complete();
                      growl.error("Error Could not update");
                      // $scope.Reg = "Error Could not Register";
                  });

        }

        $scope.ShowLogin = function () {
            $scope.loginForm = false;
            $scope.rForm = true;
        }

        $scope.ShowRegister = function () {
            $scope.loginForm = true;
            $scope.rForm = false;
        }

        $scope.Edit = function () {
            $scope.lg = true;
            $scope.editForm = false;
        }

        $scope.Cancel = function () {
            $scope.lg = false;
            $scope.editForm = true;
        }

        $scope.umalogin = false;

        $scope.Session = {
            UserId: $localStorage.UserId
        }

        $scope.CreateSession = function () {
            if ($scope.Session.UserId != null) {
                $http.post('http://kulalunch.azurewebsites.net/api/Sessions', $scope.Session)
                  .then(function (response) {
                      // Request completed successfully
                      console.log(response);
                      $localStorage.SessionId = response.data["SessionId"];
                  }, function (x) {
                      // Request error
                      console.log(x);

                  });
            }
            
        }

        //select accom[animent
        $scope.acc = "Plain Rice";
        $scope.selAcc = function (a) {
            console.log(a);
            if (a == 1) {
                $scope.acc = "Plain Rice";
                //$scope.$apply();
            } else if (a == 2) {
                $scope.acc = "Rice Biryani";
                //$scope.$apply();
            } else if (a == 3) {
                $scope.acc = "Pilau";
                //$scope.$apply();
            } else if (a == 4) {
                $scope.acc = "Chapati";
                //$scope.$apply();
            }
        }

        //Refresh menu
        $scope.refreshMenu = function () {
            $scope.progressbar.start();
            $scope.items = [];
            //get menu
            $http.get('http://kulalunch.azurewebsites.net/api/menus')
                    .then(function (response) {
                        // Request completed successfully
                        console.log(response);
                        $scope.progressbar.complete();
                        $scope.items = response.data;
                    }, function (x) {
                        // Request error
                        console.log(x);
                        $scope.progressbar.complete();
                        growl.error("Could not retrieve menu items");
                    });
        }

        $scope.Logout = function () {
            $scope.UM = "Login";
            $localStorage.$reset();
            $scope.lg = true;
            $scope.loginForm = false;
            $scope.rForm = true;
        }

        $scope.submitOrders = function () {
            $scope.progressbar.start();
            var d = new Date();
            if ($localStorage.UserId == null) {
                $scope.progressbar.complete();
                growl.error("Kindly register/Login to make order");
            } else {
                //if ($localStorage.SessionId != null) {
                if (d.getHours() >= 1 && d.getHours() <= 11) {
                //if (1 == 1 ) {
                    var p;
                    var pp = $localStorage.PhoneNumber
                    if (pp.charAt(0) === '0' || pp.charAt(0) === '+') {

                        if (!pp.charAt(0) === '+') {
                            $localStorage.PhoneNumber = "254" + pp.slice(1);
                        } 
                    }                  
                    //console.log(p);
                $http.post('http://kulalunch.azurewebsites.net/api/PostMultipleOrders?phone=' + $localStorage.PhoneNumber, $scope.mOrders)
                      .then(function (response) {
                          // Request completed successfully
                          $scope.progressbar.complete();
                          growl.success("Order submitted");
                          //$scope.Reg = "Welcome";
                          //$scope.rForm = false;
                          //$scope.loginForm = true;
                          //$scope.UMA = "LogOut";
                          //$scope.uma = true;
                      }, function (x) {
                          // Request error
                          console.log(x);
                          $scope.progressbar.complete();
                          growl.error("Could not submit");
                      });
                } else {
                    $scope.progressbar.complete();
                    growl.error("Orders are only placed before 11 am");
                }
                
            }
        }

        $scope.Login = function (UserLogin) {
            $scope.progressbar.start();
            $http.post('http://kulalunch.azurewebsites.net/api/Login', UserLogin)
                  .then(function (response) {
                      // Request completed successfully
                      $scope.progressbar.complete();
                      console.log(response);
                      $localStorage.FirstName = response.data["FirstName"];
                      $localStorage.LastName = response.data["LastName"];
                      $localStorage.BuildingName = response.data["LocationName"];
                      $localStorage.FloorNo = response.data["FloorNo"];
                      $localStorage.Email = response.data["Email"];
                      $localStorage.Office = response.data["Office"];
                      $localStorage.PhoneNumber = response.data["PhoneNumber"];
                      $localStorage.UserId = response.data["UserId"];
                      $localStorage.Latitude = response.data["Latitude"];
                      $localStorage.Longitude = response.data["Longitude"];
                      $scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };

                      $scope.fn = $localStorage.FirstName;
                      $scope.ln = $localStorage.LastName;
                      $scope.bn = $localStorage.BuildingName;
                      $scope.of = $localStorage.Office;
                      $scope.fno = $localStorage.FloorNo;
                      $scope.lg = false;
                      $scope.loginForm = true;
                      $scope.rForm = true;

                      $scope.CreateSession();

                      $scope.UM = "Logout";

                      growl.success("Welcome");
                      //$scope.Reg = "Welcome";
                      //$scope.rForm = false;
                      //$scope.loginForm = true;
                      //$scope.UMA = "LogOut";
                      //$scope.uma = true;
                  }, function (x) {
                      // Request error
                      $scope.progressbar.complete();
                      console.log(x);
                      growl.error("Could not login");
                  });
            
        }

    })
.controller("loginController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory, $window) {
    //instance of progress bar
    $scope.progressbar = ngProgressFactory.createInstance();
   


   

    $scope.UserLogin = {
        PhoneNumber: "",
        Password: ""
    }


    $scope.Session = {
        UserId: $localStorage.UserId
    }

    $scope.CreateSession = function () {
        if ($scope.Session.UserId != null) {
            $http.post('http://kulalunch.azurewebsites.net/api/Sessions', $scope.Session)
              .then(function (response) {
                  // Request completed successfully
                  console.log(response);

              }, function (x) {
                  // Request error
                  console.log(x);

              });
        }

    }

   

    

   

    

    $scope.Login = function (UserLogin) {
        $scope.progressbar.start();
        $http.post('http://kulalunch.azurewebsites.net/api/Login', UserLogin)
              .then(function (response) {
                  // Request completed successfully
                  $scope.progressbar.complete();
                  console.log(response);
                  $localStorage.FirstName = response.data["FirstName"];
                  $localStorage.LastName = response.data["LastName"];
                  $localStorage.BuildingName = response.data["LocationName"];
                  $localStorage.FloorNo = response.data["FloorNo"];
                  $localStorage.Email = response.data["Email"];
                  $localStorage.Office = response.data["Office"];
                  $localStorage.PhoneNumber = response.data["PhoneNumber"];
                  $localStorage.UserId = response.data["UserId"];
                  $localStorage.Latitude = response.data["Latitude"];
                  $localStorage.Longitude = response.data["Longitude"];
                  //$scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };

                  $scope.fn = $localStorage.FirstName;
                  $scope.ln = $localStorage.LastName;
                  $scope.bn = $localStorage.BuildingName;
                  $scope.of = $localStorage.Office;
                  $scope.fno = $localStorage.FloorNo;

                  //$scope.CreateSession();


                  growl.success("Welcome");
                  var url = "http://" + $window.location.host + "/portal/dashboard.html";
                  $log.log(url);
                  $window.location.href = url;

              }, function (x) {
                  // Request error
                  $scope.progressbar.complete();
                  console.log(x);
                  growl.error("Could not login");
              });

    }

})
.controller("portalController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory) {
    //instance of progress bar
    $scope.progressbar = ngProgressFactory.createInstance();
    //check for session
    if ($localStorage.UserId == null) {
        //$scope.UM = "REGISTER/LOGIN";
    } else {
        //check session if expired!
        $scope.progressbar.start();
        $http.get('http://kulalunch.azurewebsites.net/api/SessionsByUserId?id=' + $localStorage.UserId)
             .then(function (response) {
                 // Request completed successfully
                 console.log(response);
                 $localStorage.SessionId = response.data["SessionId"];
                 // $scope.UM = "LOGOUT";
                 $scope.fn = $localStorage.FirstName;
                 $scope.ln = $localStorage.LastName;
                 $scope.bn = $localStorage.BuildingName;
                 $scope.of = $localStorage.Office;
                 $scope.fno = $localStorage.FloorNo;
                 //$scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };
                 //$scope.lg = false;
                 //$scope.loginForm = true;
                 //$scope.rForm = true;
                 $scope.progressbar.complete();
                 //toastr.success('Welcome back!', 'Toastr fun!');
                 //growl.info("Welcome back " + $localStorage.FirstName);
             }, function (x) {
                 // Request error
                 console.log(x);
                 $scope.lg = true;
                 $scope.loginForm = false;
                 $scope.rForm = true;
                 $scope.UM = "LOGIN";
                 $scope.progressbar.complete();
                 //growl.warning("Session has expired, please login again");
             });
    }

    //orderlist
    $scope.orderItems = [];
    $scope.on = 0;

    //get menu
    //$http.get('http://kulalunch.azurewebsites.net/api/menus')
    //        .then(function (response) {
    //            // Request completed successfully
    //            console.log(response);
    //            $scope.items = response.data;
    //        }, function (x) {
    //            // Request error
    //            console.log(x);
    //            growl.error("Could not retrieve menu items");
    //        });

    $http.get('http://kulalunch.azurewebsites.net/api/Portal')
            .then(function (response) {
                // Request completed successfully
                console.log(response);
                $scope.Users = response.data["Users"];
                $scope.Menu = response.data["Menu"];
                $scope.Orders = response.data["Orders"];
                $scope.todaysOrders = response.data["todaysOrders"];
            }, function (x) {
                // Request error
                console.log(x);
                // growl.error("Could not retrieve  items");
            });

    $http.get('http://kulalunch.azurewebsites.net/api/TodaysOrders')
            .then(function (response) {
                // Request completed successfully
                console.log(response);
                $scope.items = response.data;
            }, function (x) {
                // Request error
                console.log(x);
                growl.error("Could not retrieve  items");
            });
    

   





   

    $scope.Logout = function () {
        $scope.UM = "Login";
        $localStorage.$reset();
        $scope.lg = true;
        $scope.loginForm = false;
        $scope.rForm = true;
    }

    

    

})
.controller("tdController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory, $filter, Excel, $timeout) {
    //instance of progress bar
    $scope.progressbar = ngProgressFactory.createInstance();
    //check for session
    if ($localStorage.UserId == null) {
        //$scope.UM = "REGISTER/LOGIN";
    } else {
        //check session if expired!
        $scope.progressbar.start();
        $http.get('http://kulalunch.azurewebsites.net/api/SessionsByUserId?id=' + $localStorage.UserId)
             .then(function (response) {
                 // Request completed successfully
                 console.log(response);
                 $localStorage.SessionId = response.data["SessionId"];
                 // $scope.UM = "LOGOUT";
                 $scope.fn = $localStorage.FirstName;
                 $scope.ln = $localStorage.LastName;
                 $scope.bn = $localStorage.BuildingName;
                 $scope.of = $localStorage.Office;
                 $scope.fno = $localStorage.FloorNo;
                 $scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };
                 //$scope.lg = false;
                 //$scope.loginForm = true;
                 //$scope.rForm = true;
                 $scope.progressbar.complete();
                 growl.info("Welcome back " + $localStorage.FirstName);
             }, function (x) {
                 // Request error
                 console.log(x);
                 $scope.lg = true;
                 $scope.loginForm = false;
                 $scope.rForm = true;
                 $scope.UM = "LOGIN";
                 $scope.progressbar.complete();
                 growl.warning("Session has expired, please login again");
             });
    }

    //orderlist
    $scope.orderItems = [];
    $scope.on = 0;

    //get menu
    //$http.get('http://kulalunch.azurewebsites.net/api/menus')
    //        .then(function (response) {
    //            // Request completed successfully
    //            console.log(response);
    //            $scope.items = response.data;
    //        }, function (x) {
    //            // Request error
    //            console.log(x);
    //            growl.error("Could not retrieve menu items");
    //        });

    var selected;
    $("#datepicker").datepicker({
        dateFormat: 'yyyy-mm-dd'
    }).on("change", function () {
        selected = $(this).val();
        //alert($filter('date')(selected, 'yyyy-MM-dd'));
        var da = $filter('date')(selected, 'yyyy-MM-dd');
        console.log(da);
    });

    $scope.downloadPdf = function () {
        pdfMake.createPdf(docDefinition).download();
    };

    $scope.changedate = function () {
        console.log(selected);
        $scope.items = $filter('filter')($scope.items, { Timestamp: selected });
    }

    $scope.filterOffice = function (office) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { LocationName: office });
    }

    $scope.filterBuldingName = function (building) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { BuldingName: building });
    }

    $scope.filterName = function (foodname) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { Name: foodname });
    }

    $scope.filterFullnames = function (fullnames) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { FullNames: fullnames });
    }

    $scope.refreshList = function () {
        $scope.items = $scope.Defaultitems;
    }


        $scope.exportToExcel = function (tableId) { // ex: '#my-table'
            var exportHref = Excel.tableToExcel(tableId, 'OrdersExport');
            $timeout(function () { location.href = exportHref; }, 100); // trigger download
        }
    





    $http.get('http://kulalunch.azurewebsites.net/api/TodaysOrders')
            .then(function (response) {
                // Request completed successfully
                console.log(response);
                $scope.Defaultitems = response.data;
                $scope.items = $scope.Defaultitems;
            }, function (x) {
                // Request error
                console.log(x);
                growl.error("Could not retrieve  items");
            });











    $scope.Logout = function () {
        $scope.UM = "Login";
        $localStorage.$reset();
        $scope.lg = true;
        $scope.loginForm = false;
        $scope.rForm = true;
    }

    

})

.controller("ordersController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory, $filter, Excel, $timeout) {
    //instance of progress bar
    $scope.progressbar = ngProgressFactory.createInstance();
    //check for session
    if ($localStorage.UserId == null) {
        //$scope.UM = "REGISTER/LOGIN";
    } else {
        //check session if expired!
        $scope.progressbar.start();
        $http.get('http://kulalunch.azurewebsites.net/api/SessionsByUserId?id=' + $localStorage.UserId)
             .then(function (response) {
                 // Request completed successfully
                 console.log(response);
                 $localStorage.SessionId = response.data["SessionId"];
                 // $scope.UM = "LOGOUT";
                 $scope.fn = $localStorage.FirstName;
                 $scope.ln = $localStorage.LastName;
                 $scope.bn = $localStorage.BuildingName;
                 $scope.of = $localStorage.Office;
                 $scope.fno = $localStorage.FloorNo;
                 $scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };
                 //$scope.lg = false;
                 //$scope.loginForm = true;
                 //$scope.rForm = true;
                 $scope.progressbar.complete();
                 growl.info("Welcome back " + $localStorage.FirstName);
             }, function (x) {
                 // Request error
                 console.log(x);
 
                 $scope.progressbar.complete();
                 growl.warning("Session has expired, please login again");
             });
    }


    //get menu
    //$http.get('http://kulalunch.azurewebsites.net/api/menus')
    //        .then(function (response) {
    //            // Request completed successfully
    //            console.log(response);
    //            $scope.items = response.data;
    //        }, function (x) {
    //            // Request error
    //            console.log(x);
    //            growl.error("Could not retrieve menu items");
    //        });

    $http.get('http://kulalunch.azurewebsites.net/api/Orders')
            .then(function (response) {
                // Request completed successfully
                console.log(response);
                $scope.Defaultitems = response.data;
                $scope.items = $scope.Defaultitems;
            }, function (x) {
                // Request error
                console.log(x);
                growl.error("Could not retrieve  items");
            });



    var selected;
    $("#datepicker").datepicker({
        dateFormat: 'yyyy-mm-dd'
    }).on("change", function () {
        selected = $(this).val();
        //alert($filter('date')(selected, 'yyyy-MM-dd'));
        var da = $filter('date')(selected, 'yyyy-MM-dd');
        console.log(da);
    });

    $scope.downloadPdf = function () {
        pdfMake.createPdf(docDefinition).download();
    };

    $scope.changedate = function () {
        console.log(selected);
        $scope.items = $filter('filter')($scope.items, { Timestamp: selected });
    }

    $scope.filterOffice = function (office) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { LocationName: office });
    }

    $scope.filterBuldingName = function (building) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { BuldingName: building });
    }

    $scope.filterName = function (foodname) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { Name: foodname });
    }

    $scope.filterFullnames = function (fullnames) {
        $scope.items = $scope.Defaultitems;
        $scope.items = $filter('filter')($scope.items, { FullNames: fullnames });
    }

    $scope.refreshList = function () {
        $scope.items = $scope.Defaultitems;
    }


    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'OrdersExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    }








    $scope.Logout = function () {
        $scope.UM = "Login";
        $localStorage.$reset();
        $scope.lg = true;
        $scope.loginForm = false;
        $scope.rForm = true;
    }





})


    .controller("usersController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory) {
        //instance of progress bar
        $scope.progressbar = ngProgressFactory.createInstance();
        //check for session
        if ($localStorage.UserId == null) {
            //$scope.UM = "REGISTER/LOGIN";
        } else {
            //check session if expired!
            $scope.progressbar.start();
            $http.get('http://kulalunch.azurewebsites.net/api/SessionsByUserId?id=' + $localStorage.UserId)
                 .then(function (response) {
                     // Request completed successfully
                     console.log(response);
                     $localStorage.SessionId = response.data["SessionId"];
                     // $scope.UM = "LOGOUT";
                     $scope.fn = $localStorage.FirstName;
                     $scope.ln = $localStorage.LastName;
                     $scope.bn = $localStorage.BuildingName;
                     $scope.of = $localStorage.Office;
                     $scope.fno = $localStorage.FloorNo;
                     $scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };
                     //$scope.lg = false;
                     //$scope.loginForm = true;
                     //$scope.rForm = true;
                     $scope.progressbar.complete();
                     growl.info("Welcome back " + $localStorage.FirstName);
                 }, function (x) {
                     // Request error
                     console.log(x);

                     $scope.progressbar.complete();
                     growl.warning("Session has expired, please login again");
                 });
        }


        //get menu
        //$http.get('http://kulalunch.azurewebsites.net/api/menus')
        //        .then(function (response) {
        //            // Request completed successfully
        //            console.log(response);
        //            $scope.items = response.data;
        //        }, function (x) {
        //            // Request error
        //            console.log(x);
        //            growl.error("Could not retrieve menu items");
        //        });

        $http.get('http://kulalunch.azurewebsites.net/api/Users')
                .then(function (response) {
                    // Request completed successfully
                    console.log(response);
                    $scope.items = response.data;
                }, function (x) {
                    // Request error
                    console.log(x);
                    growl.error("Could not retrieve  items");
                });











        $scope.Logout = function () {
            $scope.UM = "Login";
            $localStorage.$reset();
            $scope.lg = true;
            $scope.loginForm = false;
            $scope.rForm = true;
        }





    })


.controller("menuController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory) {
    //instance of progress bar
    $scope.progressbar = ngProgressFactory.createInstance();
    //check for session
    if ($localStorage.UserId == null) {
        //$scope.UM = "REGISTER/LOGIN";
    } else {
        //check session if expired!
        $scope.progressbar.start();
        $http.get('http://kulalunch.azurewebsites.net/api/SessionsByUserId?id=' + $localStorage.UserId)
             .then(function (response) {
                 // Request completed successfully
                 console.log(response);
                 $localStorage.SessionId = response.data["SessionId"];
                 // $scope.UM = "LOGOUT";
                 $scope.fn = $localStorage.FirstName;
                 $scope.ln = $localStorage.LastName;
                 $scope.bn = $localStorage.BuildingName;
                 $scope.of = $localStorage.Office;
                 $scope.fno = $localStorage.FloorNo;
                 $scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };
                 //$scope.lg = false;
                 //$scope.loginForm = true;
                 //$scope.rForm = true;
                 $scope.progressbar.complete();
                 growl.info("Welcome back " + $localStorage.FirstName);
             }, function (x) {
                 // Request error
                 console.log(x);
                 $scope.lg = true;
                 $scope.loginForm = false;
                 $scope.rForm = true;
                 $scope.UM = "LOGIN";
                 $scope.progressbar.complete();
                 growl.warning("Session has expired, please login again");
             });
    }

    //orderlist
    $scope.orderItems = [];
    $scope.on = 0;

    //get menu
    //$http.get('http://kulalunch.azurewebsites.net/api/menus')
    //        .then(function (response) {
    //            // Request completed successfully
    //            console.log(response);
    //            $scope.items = response.data;
    //        }, function (x) {
    //            // Request error
    //            console.log(x);
    //            growl.error("Could not retrieve menu items");
    //        });

    $http.get('http://kulalunch.azurewebsites.net/api/Menus')
            .then(function (response) {
                // Request completed successfully
                console.log(response);
                $scope.items = response.data;
            }, function (x) {
                // Request error
                console.log(x);
                growl.error("Could not retrieve  items");
            });











    $scope.Logout = function () {
        $scope.UM = "Login";
        $localStorage.$reset();
        $scope.lg = true;
        $scope.loginForm = false;
        $scope.rForm = true;
    }





})

.controller("smsController", function ($scope, $rootScope, $log, $localStorage, $http, growl, ngProgressFactory, $filter, Excel, $timeout) {
    //instance of progress bar
    $scope.progressbar = ngProgressFactory.createInstance();
    //check for session
    if ($localStorage.UserId == null) {
        //$scope.UM = "REGISTER/LOGIN";
    } else {
        //check session if expired!
        $scope.progressbar.start();
        $http.get('http://kulalunch.azurewebsites.net/api/SessionsByUserId?id=' + $localStorage.UserId)
             .then(function (response) {
                 // Request completed successfully
                 console.log(response);
                 $localStorage.SessionId = response.data["SessionId"];
                 // $scope.UM = "LOGOUT";
                 $scope.fn = $localStorage.FirstName;
                 $scope.ln = $localStorage.LastName;
                 $scope.bn = $localStorage.BuildingName;
                 $scope.of = $localStorage.Office;
                 $scope.fno = $localStorage.FloorNo;
                 $scope.map = { center: { latitude: $localStorage.Latitude, longitude: $localStorage.Longitude }, zoom: 15 };
                 //$scope.lg = false;
                 //$scope.loginForm = true;
                 //$scope.rForm = true;
                 $scope.progressbar.complete();
                 growl.info("Welcome back " + $localStorage.FirstName);
             }, function (x) {
                 // Request error
                 console.log(x);
                 $scope.lg = true;
                 $scope.loginForm = false;
                 $scope.rForm = true;
                 $scope.UM = "LOGIN";
                 $scope.progressbar.complete();
                 growl.warning("Session has expired, please login again");
             });
    }

    $http.get('http://kulalunch.azurewebsites.net/api/Users')
                 .then(function (response) {
                     // Request completed successfully
                     $scope.allUsers = response.data;

                     $scope.progressbar.complete();
                     growl.info("All Users list updated");
                 }, function (x) {
                     // Request error
                     console.log(x);

                     $scope.progressbar.complete();
                     growl.warning("Could not update Users List, Please try again");
                 });


    $scope.postSMS = function (smsMessage) {
        $http.post('http://kulalunch.azurewebsites.net/api/SMS/sendBulkSMS?message=' + smsMessage, $scope.allUsers)
                  .then(function (response) {
                      // Request completed successfully
                      $scope.result = response.data;

                      $scope.progressbar.complete();
                      growl.info($scope.result);
                  }, function (x) {
                      // Request error
                      console.log(x);

                      $scope.progressbar.complete();
                      growl.warning(x);
                  });
    }

   




    










    $scope.Logout = function () {
        $scope.UM = "Login";
        $localStorage.$reset();
        $scope.lg = true;
        $scope.loginForm = false;
        $scope.rForm = true;
    }



})
