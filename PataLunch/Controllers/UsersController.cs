﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using KulaLunch.Models;
using KulaLunch.ViewModel;
using KulaLunch.Services;

namespace KulaLunch.Controllers
{
    public class UsersController : ApiController
    {
        private KulaLunchContext db = new KulaLunchContext();

        // GET: api/Users
        public IQueryable<User> GetUsers()
        {
            return db.Users;
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(Guid id, UserVM uservm)
        {
            User user = new User()
            {
                UserId = id,
                Email = uservm.Email,
                FirstName = uservm.FirstName,
                FloorNo = uservm.FloorNo,
                LastName = uservm.LastName,
                Latitude = uservm.Latitude,
                LocationName = uservm.LocationName,
                Longitude = uservm.Longitude,
                Office = uservm.Office,
                Password = uservm.Password,
                PhoneNumber = uservm.PhoneNumber,
                TimeStamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserId)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //GET: api/ForgotPassword
        [HttpPost]
        [Route("api/ForgotPassword")]
        [ResponseType(typeof(User))]
        public async Task<HttpResponseMessage> ForgotPassword(string PhoneNumber)
        {
            var user = db.Users.Where(u => u.PhoneNumber == PhoneNumber ).FirstOrDefault();
            if (user != null)
            {
                //send email
                return Request.CreateResponse(HttpStatusCode.OK, "Email Sent!");
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, PhoneNumber);
        }

        //POST: api/Login
        [HttpPost]
        [Route("api/Login")]
        [ResponseType(typeof(User))]
        public async Task<HttpResponseMessage> Login(LoginVM loginvm)
        {
            var user = db.Users.Where(u => u.PhoneNumber == loginvm.PhoneNumber && u.Password == loginvm.Password).FirstOrDefault();
            if (user != null)
            {
                LoginResults lr = new LoginResults()
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    FloorNo = user.FloorNo,
                    Latitude = user.Latitude,
                    LocationName = user.LocationName,
                    Longitude = user.Longitude,
                    Office = user.Office
                };
                return Request.CreateResponse(HttpStatusCode.OK, lr);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, loginvm);
        }

        

        // POST: api/Users
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(UserVM uservm)
        {
            User user = new User()
            {
                UserId = Guid.NewGuid(),
                Email = uservm.Email,
                FirstName = uservm.FirstName,
                FloorNo = uservm.FloorNo,
                LastName = uservm.LastName,
                Latitude = uservm.Latitude,
                LocationName = uservm.LocationName,
                Longitude = uservm.Longitude,
                Office = uservm.Office,
                Password = uservm.Password,
                PhoneNumber = uservm.PhoneNumber,
                TimeStamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(user);

            try
            {
                await db.SaveChangesAsync();
                SendSMS sRS = new SendSMS();
                sRS.sendRegisterSMS(user.PhoneNumber);
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.UserId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = user.UserId }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(Guid id)
        {
            return db.Users.Count(e => e.UserId == id) > 0;
        }
    }
}