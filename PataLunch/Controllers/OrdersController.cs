﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using KulaLunch.Models;
using KulaLunch.ViewModel;
using KulaLunch.Services;

namespace KulaLunch.Controllers
{
    public class OrdersController : ApiController
    {
        private KulaLunchContext db = new KulaLunchContext();

        // GET: api/Orders
        public IQueryable<Order> GetOrders()
        {
            return db.Orders;
        }

        // GET: api/TodaysOrders
        [HttpGet]
        [Route("api/TodaysOrders")]
        public IQueryable<Order> TodaysOrders()
        {
            DateTime d = DateTime.Now;
            var orders = db.Orders.Where(or => or.Timestamp.Day == d.Day);          
            return orders;
        }

        // GET: api/TodaysOrders
        [HttpGet]
        [Route("api/History")]
        public HttpResponseMessage History(string names)
        {
            var orders = db.Orders.Where(or => or.FullNames == names && or.Timestamp.Day == DateTime.Today.Day);
            if (orders.Count() == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
                return Request.CreateResponse(HttpStatusCode.OK, orders);
            
        }

        // GET: api/Orders/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> GetOrder(Guid id)
        {
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrder(Guid id, OrderVM ordervm)
        {
            Order order = new Order()
            {
                OrderId = id,
                SessionId = ordervm.SessionId,
                FullNames = ordervm.FullNames,
                BuldingName = ordervm.BuldingName,
                Description = ordervm.Description,
                LocationName = ordervm.LocationName,
                Name = ordervm.Name,
                Price = ordervm.Price,
                Paid = 0,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.OrderId)
            {
                return BadRequest();
            }

            db.Entry(order).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [Route("api/PostMultipleOrders")]
        [ResponseType(typeof(List<Order>))]
        public async Task<IHttpActionResult> PostMultipleOrders(List<OrderVM> viewSE, string phone)
        {

            OrderVM ps = viewSE[0];

            foreach (var st in viewSE)
            {
                Order nST = new Order()
                {
                    OrderId = Guid.NewGuid(),
                    BuldingName = st.BuldingName,
                    Description = st.Description,
                    FullNames = st.FullNames,
                    LocationName = st.LocationName,
                    Name = st.Name,
                    Paid = st.Paid,
                    Price = st.Price,
                    SessionId = st.SessionId,
                    Timestamp = DateTime.Now

                };

                db.Orders.Add(nST);
                db.SaveChanges();
                
            }
            SendSMS ss = new SendSMS();
            ss.sendSMS(phone);
            return Ok("Order Added");
        }

        // POST: api/Orders
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> PostOrder(OrderVM ordervm)
        {
            Order order = new Order()
            {
                OrderId = Guid.NewGuid(),
                SessionId = ordervm.SessionId,
                FullNames = ordervm.FullNames,
                BuldingName = ordervm.BuldingName,
                Description = ordervm.Description,
                LocationName = ordervm.LocationName,
                Name = ordervm.Name,
                Price = ordervm.Price,
                Paid = 0,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Orders.Add(order);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrderExists(order.OrderId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = order.OrderId }, order);
        }

        // DELETE: api/Orders/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> DeleteOrder(Guid id)
        {
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            db.Orders.Remove(order);
            await db.SaveChangesAsync();

            return Ok(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(Guid id)
        {
            return db.Orders.Count(e => e.OrderId == id) > 0;
        }
    }
}