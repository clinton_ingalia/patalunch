﻿using KulaLunch.Models;
using KulaLunch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KulaLunch.Controllers
{
    [RoutePrefix("api/SMS")]
    public class SMSController : ApiController
    {
        private KulaLunchContext db = new KulaLunchContext();

        [HttpPost]
        [Route("sendBulkSMS")]
        public HttpResponseMessage sendBulkSMS(string message, List<User> users)
        {
            if (users != null)
            {
                foreach (var user in users)
                {
                    string phone = "254" + user.PhoneNumber.Substring(1);

                    SendBulkSMS ss = new SendBulkSMS();
                    ss.sendBulkSMS(phone, message);

                }
                return Request.CreateResponse(HttpStatusCode.OK, "Message sent");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "0 users");
            }

        }
    }
}
