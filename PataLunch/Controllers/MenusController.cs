﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using KulaLunch.Models;
using KulaLunch.ViewModel;

namespace KulaLunch.Controllers
{
    public class MenusController : ApiController
    {
        private KulaLunchContext db = new KulaLunchContext();

        // GET: api/Menus
        public List<Menu> GetMenus()
        {
            //check the date
            string dayOfWeek = DateTime.Now.DayOfWeek.ToString();
            if (dayOfWeek != "Friday")
            {
                var menuItems = db.Menus.ToList();
                var itemToRemove = menuItems.SingleOrDefault(m => m.MenuId.ToString() == "f3db6bed-b87d-4cd5-b0ae-8fbc5e34ca1c");
                menuItems.Remove(itemToRemove);
                return menuItems;
            }
            else
            {
                return db.Menus.ToList();
            }
            
        }

        // GET: api/Menus/5
        [ResponseType(typeof(Menu))]
        public async Task<IHttpActionResult> GetMenu(Guid id)
        {
            Menu menu = await db.Menus.FindAsync(id);
            if (menu == null)
            {
                return NotFound();
            }

            return Ok(menu);
        }

        // PUT: api/Menus/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMenu(Guid id, Menu menu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != menu.MenuId)
            {
                return BadRequest();
            }

            db.Entry(menu).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MenuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Menus
        [ResponseType(typeof(Menu))]
        public async Task<IHttpActionResult> PostMenu(MenuVM menuvm)
        {
            Menu menu = new Menu()
            {
                MenuId = Guid.NewGuid(),
                Name = menuvm.Name,
                Description = menuvm.Description,
                Price = menuvm.Price,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Menus.Add(menu);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MenuExists(menu.MenuId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = menu.MenuId }, menu);
        }

        // DELETE: api/Menus/5
        [ResponseType(typeof(Menu))]
        public async Task<IHttpActionResult> DeleteMenu(Guid id)
        {
            Menu menu = await db.Menus.FindAsync(id);
            if (menu == null)
            {
                return NotFound();
            }

            db.Menus.Remove(menu);
            await db.SaveChangesAsync();

            return Ok(menu);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MenuExists(Guid id)
        {
            return db.Menus.Count(e => e.MenuId == id) > 0;
        }
    }
}