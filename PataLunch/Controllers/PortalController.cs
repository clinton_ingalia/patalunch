﻿using KulaLunch.Models;
using KulaLunch.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KulaLunch.Controllers
{
    public class PortalController : ApiController
    {
        private KulaLunchContext db = new KulaLunchContext();

        [HttpGet]
        [Route("api/Portal")]
        public PortalStats PortalStatistics()
        {
            DateTime d = DateTime.Now;
            
            PortalStats ps = new PortalStats()
            {
                Users = db.Users.Count(),
                Menu = db.Menus.Count(),
                Orders = db.Orders.Count(),
                todaysOrders = db.Orders.Where(o => o.Timestamp.Day == d.Day).Count()
            };
            return ps;
        }
    }
}
