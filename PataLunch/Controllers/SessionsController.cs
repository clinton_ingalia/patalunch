﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using KulaLunch.Models;
using KulaLunch.ViewModel;

namespace KulaLunch.Controllers
{
    public class SessionsController : ApiController
    {
        private KulaLunchContext db = new KulaLunchContext();

        // GET: api/Sessions
        public IQueryable<Session> GetSessions()
        {
            return db.Sessions;
        }

        // GET: api/Sessions/5
        [ResponseType(typeof(Session))]
        public async Task<IHttpActionResult> GetSession(Guid id)
        {
            Session session = await db.Sessions.FindAsync(id);
            if (session == null)
            {
                return NotFound();
            }

            return Ok(session);
        }

        // GET: api/Sessions/5
        [HttpGet]
        [Route("api/SessionsByUserId")]  
        [ResponseType(typeof(Session))]
        public async Task<HttpResponseMessage> SessionsByUserId(Guid id)
        {
            List<Session> sessionList = await db.Sessions.Where(s => s.UserId == id).ToListAsync(); 
            if (sessionList == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, "No Session Found");
            }
            else
            {
                foreach (var item in sessionList)
                {
                    TimeSpan t = item.Timestamp - DateTime.Now;
                    if (t.TotalHours <= 24)
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, item);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, "Session Expired");
            }

        }

        // PUT: api/Sessions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSession(Guid id, SessionVM sessionvm)
        {
            Session session = new Session()
            {
                SessionId = Guid.NewGuid(),
                UserId = sessionvm.UserId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != session.SessionId)
            {
                return BadRequest();
            }

            db.Entry(session).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SessionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sessions
        [ResponseType(typeof(Session))]
        public async Task<IHttpActionResult> PostSession(SessionVM sessionvm)
        {
            Session session = new Session()
            {
                SessionId = Guid.NewGuid(),
                UserId = sessionvm.UserId,
                Timestamp = DateTime.Now
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<Session> sessionList = await db.Sessions.Where(s => s.UserId == sessionvm.UserId).ToListAsync();
            if (sessionList != null)
            {
                foreach (var item in sessionList)
                {
                    TimeSpan t = item.Timestamp - DateTime.Now;
                    if (t.TotalHours <= 24)
                    {
                        return Conflict();
                    }
                }
            }
            

            db.Sessions.Add(session);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SessionExists(session.SessionId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = session.SessionId }, session);
        }

        // DELETE: api/Sessions/5
        [ResponseType(typeof(Session))]
        public async Task<IHttpActionResult> DeleteSession(Guid id)
        {
            Session session = await db.Sessions.FindAsync(id);
            if (session == null)
            {
                return NotFound();
            }

            db.Sessions.Remove(session);
            await db.SaveChangesAsync();

            return Ok(session);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SessionExists(Guid id)
        {
            return db.Sessions.Count(e => e.SessionId == id) > 0;
        }
    }
}