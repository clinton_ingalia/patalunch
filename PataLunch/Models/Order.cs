﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KulaLunch.Models
{
    public class Order
    {
        [Key]
        public Guid OrderId { get; set; }
        public Guid SessionId { get; set; }
        public string FullNames { get; set; }
        public string LocationName { get; set; }
        public string BuldingName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Paid { get; set; }
        public DateTime Timestamp { get; set; }
    }
}