﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KulaLunch.Models
{
    public class Session
    {
        [Key]
        public Guid SessionId { get; set; }
        public Guid UserId { get; set; }
        //public bool Ordered { get; set; }
        public DateTime Timestamp { get; set; }
    }
}