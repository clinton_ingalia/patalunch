﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulaLunch.Services
{
    public class SendBulkSMS
    {
        public void sendBulkSMS(string phone, string message)
        {
            // Specify your login credentials
            string username = "INGALIA";
            string apiKey = "168d45e0386640d7240c22662abf0268a62a49ab5abd6b2352d36e3e4420b452";

            // Specify the numbers that you want to send to in a comma-separated list
            // Please ensure you include the country code (+254 for Kenya in this case)
            string recipients = phone;
            // And of course we want our recipients to know what we really do

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {
                // Thats it, hit send and we'll take care of the rest

                dynamic results = gateway.sendMessage(recipients, message);



                foreach (dynamic result in results)
                {
                    Console.Write((string)result["number"] + ",");
                    Console.Write((string)result["status"] + ","); // status is either "Success" or "error message"
                    Console.Write((string)result["messageId"] + ",");
                    Console.WriteLine((string)result["cost"]);
                }
            }
            catch (AfricasTalkingGatewayException e)
            {
                Console.WriteLine("Encountered an error: " + e.Message);
            }
        }
    }
}